# Installation 

Ce document montre comment installer l'application "Health Devices Control", étape par étape, en local ainsi que tous les prérequis en terme d''architecture et d'installations . 

Assurez vous de :

 - bien respecter chaque étapes
 - d'avoir tous les prérequis 
 - de bien vérifier les version des dépendances

## Prérequis
- JDK 11
- MYSQL v 5.7 ET +
- Wamp server
## Procédure d'installation

### Importation du projet
pour installer l'aaplication en local il faut tout dabord importer le code source soit en utilisant l'outil git oubien en telechargeant directement le projet depuis cette repository.
### installation de la base de données
#### Avec phpMyAdmin
1. Ouvrez phpMyAdmin
2. clickez sur importer
3. clickez SUR choisir un fichier
4. naviguez vers le dossier du projet et selectionnez le fichier db.sql
5. en fin cliquez sur executer puis laissez le script s'executer.
6. à la fin de l'execution si tout se passe bien la base de donnée hdcDB doit figurer parmi les bases de données locales.
#### Via la ligne de commande (recommandé)
1. Ouvrez la ligne de commande puis placez vous dans le repertoire du projet grace à la commande "cd chemin vers le repertoire du projet"
2. connectez vous à l'interface de mysql mia la commande :
`mysql -u"username" -p"password"`
3. Puis créez la base de données hdcDB
`create database hdcDB`
4. entrez la commande `use hdcDB`
5. entrez la commande `source ./db.sql`
> NB il faut que la ligne de commande pointe vers le dossier health-device-control pour que le script sql soit accessible.

### Configuration de l'application
####  Configuration de la base de donnée
ouvrez le avec votre ide préféré (intellij de préférance) puis ouvrez le fichier "src > main > ressources > application-dev.properties" puis modifiez le username et le password de pour mettre à la place vos identifiants (ceux que vous utilisez pour vous connecter à mysql)

#### Configuration du runtime 
Modifiez les configurations d'execution pour executer l'application depuis la classe main :"src > main > java > edu.universiapolis.hdc > HdcApplication "

### execution de l'application

lancer l'execution puis utilisez votre navigateur pour vous connecter au port 8080 du domain local (localhost) vous arriverez dans la page de connexion utilisez les identifiants suivants:
- nom d'utilisateur :test@test.com
- mot de passe: secret

#VOILA


