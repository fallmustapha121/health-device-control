package edu.universiapolis.hdc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.universiapolis.hdc.helper.RandomString;
import edu.universiapolis.hdc.model.*;
import edu.universiapolis.hdc.services.CustomUserDetailService;
import edu.universiapolis.hdc.services.RespiratorService;
import edu.universiapolis.hdc.services.StaffService;
import edu.universiapolis.hdc.services.HopitalService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.InvalidParameterException;
import java.util.*;

@Controller
public class publicController {
    private final HopitalService hopitalService;
    private final StaffService staffService;
    private final RespiratorService respiratorService;
    private final CustomUserDetailService userService;
    private final PasswordEncoder encoder;
    @Autowired
    public publicController(HopitalService hopitalService, StaffService staffService, RespiratorService respiratorService, CustomUserDetailService userService, PasswordEncoder encoder) {
        this.hopitalService = hopitalService;
        this.staffService = staffService;
        this.respiratorService = respiratorService;
        this.userService = userService;
        this.encoder = encoder;
    }

    //Login
    @GetMapping("login")
    public String getLogin(){
        return "login";
    }

    // Home
    @GetMapping("dashboard")
    public String dashboard(Model model){
        loadUser(model);
        return "dashboard";
    }
    @GetMapping("/")
    public String home(Model model){
        loadUser(model);
        return "redirect:dashboard";
    }


    //List Hospital Page
    @GetMapping("hopitals")
    public String getHopitalList(Model model,
                                 @RequestParam(required = false) Integer offset,
                                 @RequestParam(required = false) String sortBy,
                                 @RequestParam(required = false) String search){
        loadUser(model);
        if (search!=null&&search.isEmpty()){
            search=null;
        }
        List<Hopital> list;
        Page<Hopital> page =hopitalService.getAll(
                offset!=null?offset:0,
                Sort.by(sortBy!=null?sortBy:"nom"),
                search
                );
        list=page.toList();
        model.addAttribute("totalPage",page.getTotalPages());
        model.addAttribute("total",page.getTotalElements());
        model.addAttribute("hopitals",list);
        model.addAttribute("currentPage",page.getNumber());
        model.addAttribute("hasNext", page.hasNext());
        model.addAttribute("hasPrevious", page.hasPrevious());
        model.addAttribute("search","");
        return "hopital";
    }

    // Edit page
    @GetMapping("modifier")
    public String getEditPage(Model model,@RequestParam long id) throws NotFoundException {
        loadUser(model);
        Optional<Hopital> hopital=hopitalService.findOne(id);
        if (hopital.isPresent()){
            model.addAttribute("hopital",hopital.get());
            return "modification";
        }
        throw new NotFoundException("No Corresponding hospital found");

    }

    // edit processing
    @PostMapping("modifier")
    public String getEditPage(@Valid  Hopital hopital, BindingResult result ,Model model) throws NotFoundException {
        loadUser(model);
        if (!result.hasErrors()){
            if (hopitalService.exist(hopital.getId())){
                hopitalService.save(hopital);
                model.addAttribute("edited",true);
                return "redirect:hopitals";
            }
        }
        throw new NotFoundException("No Corresponding hospital found");
    }
    // AddPage
    @GetMapping("ajouter")
    public String getAddPage(Model model){
        loadUser(model);
        model.addAttribute("hopital",new Hopital());
        model.addAttribute("success",false);
        model.addAttribute("newHopital",null);
        model.addAttribute("failed",false);
        return "ajouter";
    }


    //Add processing
    @PostMapping("ajouter")
    public String addHospital(@Valid  Hopital hopital, BindingResult result ,Model model){
        loadUser(model);
        if (result.hasErrors()){
            model.addAttribute("failed",true);
            return "ajouter";
        }
        Hopital hopital1=null;
        model.addAttribute("added","true");
        model.addAttribute("sent","true");
        model.addAttribute("hopital",hopital1);
           hopital1= hopitalService.save(hopital);
        model.addAttribute("hopital",new Hopital());
        model.addAttribute("success",true);
        model.addAttribute("failed",false);
        model.addAttribute("newHopital",hopital1);
        return "ajouter";
    }


    //Find One
    @GetMapping("hopitals/{id}")
    public String getHospitalDetails(@PathVariable long id,Model model) throws NotFoundException {
        loadUser(model);
        Optional<Hopital> result=hopitalService.findOne(id);
        if (result.isPresent()){
            model.addAttribute("hopital",result.get());
            return "hopital";
        }
        throw new NotFoundException("Resource not Found");
    }

    @PostMapping("delete")
    public String deleteHospital(@RequestParam(name = "idToDelete") String id,Model model) throws NotFoundException {
        loadUser(model);
        long parsedId;
        try{
             parsedId=Long.parseLong(id);
        }catch (NumberFormatException ex){
            throw new InvalidParameterException("Bad Id");
        }
        if (hopitalService.exist(parsedId)){
            hopitalService.delete(parsedId);
            model.addAttribute("edited",true);
            return "redirect:hopitals";
        }
        throw new NotFoundException("No Corresponding hospital found");
    }
    @PostMapping("delete-staff")
    public String delStaff(@RequestParam(name = "idToDelete") String id,Model model) throws NotFoundException {
        loadUser(model);
        long parsedId;
        try{
            parsedId=Long.parseLong(id);
        }catch (NumberFormatException ex){
            throw new InvalidParameterException("Bad Id");
        }
        if (staffService.exist(parsedId)){
            staffService.delete(parsedId);
            model.addAttribute("edited",true);
            return "redirect:staff";
        }
        throw new NotFoundException("No Corresponding hospital found");
    }
    @GetMapping("staff")
    public String staff(Model model,
                        @RequestParam(required = false) Integer offset,
                        @RequestParam(required = false) String sortBy,
                        @RequestParam(required = false) String search){
        if (search!=null&&search.isEmpty()){
            search=null;
        }
        List<Staff> list;
        Page<Staff> page =staffService.getAll(
                offset!=null?offset:0,
                Sort.by(sortBy!=null?sortBy:"lastName"),
                search
        );
        list=page.toList();
        model.addAttribute("totalPage",page.getTotalPages());
        model.addAttribute("total",page.getTotalElements());
        model.addAttribute("members",list);
        model.addAttribute("currentPage",page.getNumber());
        model.addAttribute("hasNext", page.hasNext());
        model.addAttribute("hasPrevious", page.hasPrevious());
        model.addAttribute("search","");
        loadUser(model);
        return "staff";
    }
    @GetMapping("new-staff")
    public String newStaff(Model model){
        loadUser(model);
        model.addAttribute("hopitals", hopitalService.loadAll());
        model.addAttribute("doctor",new Doctor());
        model.addAttribute("staff",new SimpleStaff());
        model.addAttribute("created",false);
        model.addAttribute("failed",false);
        return "newStaff";
    }
    @PostMapping(value = "new-staff")
    @PreAuthorize("hasAuthority('staff_write')")
    public String newStaffProcessing(@RequestParam Map<String, Object> map,Model model ){
        Staff staff;
        model.addAttribute("failed",false);
        model.addAttribute("created",false);
        try {
            if (map.get("staff_type").toString().equalsIgnoreCase("doctor")) {
                Doctor doc = (Doctor) staffMapping(map, new Doctor());
                model.addAttribute("new_doctor", doc);
                doc.setUser(userService.save(initUser(map, doc, model,true)));
                staffService.save(doc);
            } else if (map.get("staff_type").toString().equalsIgnoreCase("staff")) {
                SimpleStaff simpleStaff = (SimpleStaff) staffMapping(map, new SimpleStaff());
                model.addAttribute("new_staff", simpleStaff);
                simpleStaff.setUser(userService.save(initUser(map, simpleStaff, model,true)));
                staffService.save(simpleStaff);

            }
        }catch (Exception e){
            model.addAttribute("failed",true);
        }
        model.addAttribute("doctor",new Doctor());
        model.addAttribute("staff",new SimpleStaff());
        loadUser(model);
        model.addAttribute("created",true);
        model.addAttribute("edit_success",true);
        model.addAttribute("hopitals", hopitalService.loadAll());
        return "newStaff";
    }
    private void loadUser(Model model){
        User user=(User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("firstName",user.getFirstName());
        model.addAttribute("lastName",user.getLastName());

    }
    private User initUser(Map<String,Object> map,Staff staff,Model model,boolean initMdp){
        //user unit
        User user=new User();
        if (staff instanceof Doctor){
            user.setUsername(map.get("mail").toString());
        }else if (staff instanceof SimpleStaff){
            user.setUsername(map.get("mail2").toString());
        }
        String password= new RandomString(5).nextString();
        if (initMdp){
            user.setPassword(encoder.encode(password));
        }
        user.setFirstName(staff.getFirstName());
        user.setLastName(staff.getLastName());
        user.setLocked(false);
        user.setEnabled(true);
        model.addAttribute("password",password);
        model.addAttribute("created",true);
        return user;
    }
    @GetMapping("hopitals/{hopital}/staffs/{staffId}/edit")
    public String editStaffPage(@PathVariable long hopital,@PathVariable long staffId,Model model) throws NotFoundException {
        loadUser(model);
        if (hopitalService.exist(hopital)){
            Optional<Staff> staff=staffService.findOne(staffId);
            if (staff.isPresent()){
                model.addAttribute("staff",staff.get() instanceof Doctor?(Doctor) staff.get():(SimpleStaff) staff.get());
                model.addAttribute("success",false);
                return "staffEdit";
            }
        }
        throw new NotFoundException("Resource not found");
    }

    @PostMapping(value = "hopitals/{hopital}/staffs/{staffId}/edit")
    @PreAuthorize("hasAuthority('staff_write')")
    public String editStaffProcessing(@RequestParam Map<String, Object> map,Model model ){
        Staff staff;
        System.out.println(map);
        model.addAttribute("failed",false);
        model.addAttribute("created",false);
        boolean editMdp= Boolean.parseBoolean(map.get("reset_password").toString());
        System.out.println("1");
        try {
            if (map.get("staff_type").toString().equalsIgnoreCase("Medecin")) {
                Doctor doc = (Doctor) staffMapping(map, new Doctor());
                System.out.println("2"+doc);
                doc.setId(Long.parseLong(map.get("id").toString()));
                System.out.println("3"+doc);
                model.addAttribute("new_doctor", doc);
                model.addAttribute("staff",staffService.save(doc));
            } else if (map.get("staff_type").toString().equalsIgnoreCase("Personnel")) {
                System.out.println("2");
                SimpleStaff simpleStaff = (SimpleStaff) staffMapping(map, new SimpleStaff());
                System.out.println("5"+simpleStaff);

                simpleStaff.setId(Long.parseLong(map.get("id").toString()));
                System.out.println("4"+simpleStaff);

                model.addAttribute("new_staff", simpleStaff);
                model.addAttribute("staff",staffService.save(simpleStaff));

            }
        }catch (Exception e){
            model.addAttribute("failed",true);
        }
        model.addAttribute("doctor",new Doctor());
        loadUser(model);
        model.addAttribute("created",true);
        model.addAttribute("edit_success",true);
        return "staffEdit";
    }

    @GetMapping("materiels")
    public String materielList(Model model,
                                 @RequestParam(required = false) Integer offset,
                                 @RequestParam(required = false) String sortBy,
                                 @RequestParam(required = false) String search){
        loadUser(model);
        List<Hopital> hopitals=hopitalService.loadAll();
        model.addAttribute("hopitals",hopitals);
        model.addAttribute("respirator",new Respirator());
        if (search!=null&&search.isEmpty()){
            search=null;
        }
        List<Respirator> list;
        Page<Respirator> page =respiratorService.getAll(
                offset!=null?offset:0,
                Sort.by(sortBy!=null?sortBy:"name"),
                search
        );
        list=page.toList();
        model.addAttribute("totalPage",page.getTotalPages());
        model.addAttribute("total",page.getTotalElements());
        model.addAttribute("respirators",list);
        model.addAttribute("currentPage",page.getNumber());
        model.addAttribute("hasNext", page.hasNext());
        model.addAttribute("hasPrevious", page.hasPrevious());
        model.addAttribute("search","");
        return "materiels";
    }
    @PostMapping("ajouter-materiel")
    public String addMaterial(@RequestParam Map<String,Object> map,Model model){
        loadUser(model);

        ObjectMapper mapper=new ObjectMapper();
        if (!map.containsKey("hopital")||map.get("hopital")==null){
            throw new InvalidParameterException("Hopital invalid");
        }
        Hopital hopital;

        try{
            String hs=map.get("hopital").toString();
            System.out.println(hs);
            String[] str=hs.substring(hs.indexOf("id"),hs.indexOf(")")).split(",");
            Map<String,String> hmap=new HashMap<>();
            for (String st: str
            ) {
                String[] elts=st.split("=");
                hmap.put(elts[0].trim(),elts[1].trim());
            }
            hopital=mapper.convertValue(hmap,Hopital.class);
        }catch (Exception e){
            model.addAttribute("failed",true);
            return "materiels";
        }
        Respirator respi=new Respirator();
//        validateMapProperty(map,map.get("name").toString());
//        validateMapProperty(map,map.get("room").toString());
        respi.setName(map.get("name").toString());
        respi.setRoom(map.get("room").toString());
        respi.setHopital(hopital);
        respi= respiratorService.save(respi);
        if (respi!=null){
            model.addAttribute("success",true);
        }
        model.addAttribute("respirator",new Respirator());
        model.addAttribute("failed",false);
        model.addAttribute("respirators",respiratorService.loadAll());
        return "materiels";
    }

    private Staff staffMapping(Map<String,Object> map,Staff staff){
        //hopital extraction
        ObjectMapper mapper=new ObjectMapper();
        if (!map.containsKey("hopital")||map.get("hopital")==null){
            throw new InvalidParameterException("Hopital invalid");
        }
        Hopital hopital;

        try{
            String hs=map.get("hopital").toString();
            System.out.println(hs);
            String[] str=hs.substring(hs.indexOf("id"),hs.indexOf(")")).split(",");
            Map<String,String> hmap=new HashMap<>();
            for (String st: str
            ) {
                String[] elts=st.split("=");
                hmap.put(elts[0].trim(),elts[1].trim());
            }
             hopital=mapper.convertValue(hmap,Hopital.class);
        }catch (Exception e){
            throw new InvalidParameterException("HOpital invalid");
        }
        validateMapProperty(map,"firstName");
        validateMapProperty(map,"lastName");
        validateMapProperty(map,"sex");
        //staff init
        staff.setFirstName(map.get("firstName").toString());
        staff.setLastName(map.get("lastName").toString());
        if (map.containsKey("age")&&map.get("age")!=null&&!map.get("age").toString().isEmpty()){
            staff.setAge(Integer.parseInt(map.get("age").toString()));
        }
        staff.setHopital(hopital);
        if (map.containsKey("sex")&&map.get("sex")!=null&&!map.get("sex").toString().isEmpty()){
            staff.setSex(map.get("sex").toString());
        }
        if (map.containsKey("phone")&&map.get("phone")!=null&&!map.get("phone").toString().isEmpty()){
            staff.setPhone(Integer.parseInt(map.get("phone").toString()));
        }
        staff.setType("doctor");
        if (staff instanceof Doctor){
            Doctor doctor=(Doctor) staff;
            if (map.containsKey("domain")&&map.get("domain")!=null&&!map.get("domain").toString().isEmpty()){
                doctor.setDomain(map.get("domain").toString());
            }
            if (map.containsKey("status")&&map.get("status")!=null&&!map.get("status").toString().isEmpty()){
                doctor.setStatus(map.get("status").toString());
            }
            return doctor;
        }else if (staff instanceof SimpleStaff){

            SimpleStaff simpleStaff=(SimpleStaff) staff;
            if (map.containsKey("function")&&map.get("function")!=null&&!map.get("function").toString().isEmpty()){
                simpleStaff.setFunction(map.get("function").toString());
            }
            return simpleStaff;
        }
        else return null;
    }
    private void validateMapProperty(Map<String,Object> map,String property){
        if (!map.containsKey(property)||map.get(property)==null){
            throw new InvalidParameterException("invalid"+property);
        }
    }
}
