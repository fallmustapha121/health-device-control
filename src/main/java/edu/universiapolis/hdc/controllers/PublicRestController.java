package edu.universiapolis.hdc.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("hdc/")
public class PublicRestController {
    @GetMapping("test")
    public String test(){
        return "voila";
    }
}
