package edu.universiapolis.hdc.model;

import jdk.jfr.Enabled;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Patient extends Person {
    @ManyToOne
    private Doctor doctor;
    @ManyToMany
    @JoinTable(name = "staff_has_patient")
    private Collection<Staff> responsible;
    private float weight;
    private float height;
    private String pathology;
    private Date entryDate;
    @OneToOne(mappedBy = "patient")
    private Respirator respirator;
    private String room;
}
