package edu.universiapolis.hdc.model;

public enum RespiratorMode {
    VOLUMETRIC,BAROMETRIC,HYBRID
}
