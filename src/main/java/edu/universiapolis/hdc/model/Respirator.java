package edu.universiapolis.hdc.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.sql.Time;
@Data
@NoArgsConstructor
@Entity
public class Respirator extends Device{
    @OneToOne()
    @JoinColumn(name = "patient_id")
    private Patient patient;
    private float volume;
    private float pressure;
    private Time time;
    private float pep;
    private String mode;

}

