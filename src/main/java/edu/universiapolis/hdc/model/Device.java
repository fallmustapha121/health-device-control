package edu.universiapolis.hdc.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
@Data
@MappedSuperclass
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private Hopital hopital;
    private String name;
    private String room;
    private Date startTime;
    private Date extinctionTime;
    private boolean state;

}
