package edu.universiapolis.hdc.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("other")
@Data
public class SimpleStaff extends Staff{
    public SimpleStaff(){
        this.setType("Personnel");
    }
    @Column(name = "fonction")
    private String function;
}
