package edu.universiapolis.hdc.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Data
public class Client implements ClientDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private boolean isPrivate;
    private String secret;
    private String scope;
    @ManyToMany
    @JoinTable(name = "client_has_grant_type",joinColumns = @JoinColumn(name = "Client_id"), inverseJoinColumns = @JoinColumn(name = "grant_type_id"))
    private Set<GrantTypes> grantTypes;
    @OneToMany
    private Set<RedirectUri> redirectUris;
    private Integer accessTokenValiditySeconds;
    private Integer refreshTokenValiditySeconds;
    @Override
    public String getClientId() {
        return String.valueOf(id);
    }

    @Override
    public Set<String> getResourceIds() {
        return null;
    }

    @Override
    public boolean isSecretRequired() {
        return isPrivate;
    }

    @Override
    public String getClientSecret() {
        return secret;
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public Set<String> getScope() {
        return new HashSet<String>(Arrays.asList(scope.split(" ")));
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return grantTypes.stream().map(GrantTypes::getGrantType).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return redirectUris.stream().map(RedirectUri::getUri).collect(Collectors.toSet());
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String s) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }
}
