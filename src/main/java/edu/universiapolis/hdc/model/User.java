package edu.universiapolis.hdc.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private boolean enabled;
    private boolean locked;
    private String photo;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_has_authority",joinColumns = @JoinColumn(name = "user_id"),inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Authority> authorities;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //transformation of authorizations from string to GrantedAuthority objects
        return authorities.stream().map(authority -> new GrantedAuthority() {
            @Override
            public String getAuthority(){
                return authority.getAuthority();
            }
        }).collect(Collectors.toList());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

}
