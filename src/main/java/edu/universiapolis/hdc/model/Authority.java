package edu.universiapolis.hdc.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Authority {
    @Id
    private int id;
    private String authority;
}
