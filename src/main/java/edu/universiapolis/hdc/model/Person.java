package edu.universiapolis.hdc.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@MappedSuperclass
@Data
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    @NotEmpty
    private String firstName;
    @NotNull
    @NotEmpty
    private String lastName;
    private int age;
    private long phone;
    @NotNull
    @NotEmpty
    private String sex;
    @ManyToOne
    @JoinColumn(name = "hopital_id")
    @NotNull
    private Hopital hopital;

}
