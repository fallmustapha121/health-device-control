package edu.universiapolis.hdc.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@Data
@DiscriminatorValue("doctor")
public class Doctor extends Staff{
    public Doctor(){
        this.setType("Medecin");
    }
    private String domain;
    private String status;
}
