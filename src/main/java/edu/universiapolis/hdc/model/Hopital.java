package edu.universiapolis.hdc.model;

import lombok.Data;
import lombok.Value;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Hopital {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @NotEmpty
    private String nom;
    @NotEmpty
    @NotNull
    private String adresse;
    @Email
    private String email;
    private long telephone;
}
