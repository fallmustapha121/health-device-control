package edu.universiapolis.hdc.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type",discriminatorType = DiscriminatorType.STRING)
public class Staff extends Person {
    @Transient
    private String type;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;


}
