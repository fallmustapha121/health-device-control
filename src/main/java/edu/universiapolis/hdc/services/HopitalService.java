package edu.universiapolis.hdc.services;

import edu.universiapolis.hdc.model.Hopital;
import edu.universiapolis.hdc.repositories.HopitalRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class HopitalService {
    private final HopitalRepository repository;
    private final int MAXRESULTPERPAGE=10;
    public HopitalService(HopitalRepository repository) {
        this.repository = repository;
    }


    public Hopital save(Hopital hopital){
        return repository.save(hopital);
    }

    public List<Hopital> loadAll(){
        return (List<Hopital>) repository.findAll();
    }
    public Page<Hopital> getAll( int offset, Sort sort,String search){
        if (search==null) {
            return repository.findAll(PageRequest.of(offset, MAXRESULTPERPAGE, sort));
        }
        return repository.findAllByNom(search,PageRequest.of(offset, MAXRESULTPERPAGE, sort));
    }
    public Optional<Hopital> findOne(long id) {
        return repository.findById(id);
    }
    public Boolean exist(long id){
        return repository.existsById(id);
    }

    public void delete(long id){
        repository.deleteById(id);
    }

}
