package edu.universiapolis.hdc.services;

import edu.universiapolis.hdc.model.User;
import edu.universiapolis.hdc.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailService implements UserDetailsService {
    private final UserRepository repository;
    private final PasswordEncoder encoder= PasswordEncoderFactories.createDelegatingPasswordEncoder();
    @Autowired
    public CustomUserDetailService(UserRepository repository){
        this.repository = repository;
    }
    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user=repository.findByUsername(username);
        user.orElseThrow(()->new  UsernameNotFoundException("Username or password invalid"));
        return user.get();
    }
    public User save(User user){
        return repository.save(user);
    }
}
