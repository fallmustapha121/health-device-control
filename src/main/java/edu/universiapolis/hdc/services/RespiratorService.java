package edu.universiapolis.hdc.services;

import edu.universiapolis.hdc.model.Hopital;
import edu.universiapolis.hdc.model.Respirator;
import edu.universiapolis.hdc.repositories.RespiratorRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class RespiratorService {
    private final RespiratorRepository repository;
    private final int MAXRESULTPERPAGE=10;

    public RespiratorService(RespiratorRepository repository) {
        this.repository = repository;
    }


    public Respirator save(Respirator respirator){
        return repository.save(respirator);
    }

    public List<Respirator> loadAll(){
        return (List<Respirator>) repository.findAll();
    }
    public Page<Respirator> getAll(int offset, Sort sort, String search){
        if (search==null) {
            return repository.findAll(PageRequest.of(offset, MAXRESULTPERPAGE, sort));
        }
        return repository.findAllByName(search,PageRequest.of(offset, MAXRESULTPERPAGE, sort));
    }
    public Optional<Respirator> findOne(long id) {
        return repository.findById(id);
    }
    public Boolean exist(long id){
        return repository.existsById(id);
    }

    public void delete(long id){
        repository.deleteById(id);
    }
}
