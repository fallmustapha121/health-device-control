package edu.universiapolis.hdc.services;

import edu.universiapolis.hdc.model.Doctor;
import edu.universiapolis.hdc.model.Staff;
import edu.universiapolis.hdc.repositories.StaffRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StaffService {
    private final StaffRepository repository;
    private final int MAXRESULTPERPAGE=10;
    public StaffService(StaffRepository repository) {
        this.repository = repository;
    }


    public Staff save(Staff staff){
        return repository.save(staff);
    }


    public Page<Staff> getAll(int offset, Sort sort, String search){
        if (search==null) {
            return repository.findAll(PageRequest.of(offset, MAXRESULTPERPAGE, sort));
        }
        return repository.findAllByFirstNameOrLastName(search,search,PageRequest.of(offset, MAXRESULTPERPAGE, sort));
    }
    public Optional<Staff> findOne(long id) {
        return repository.findById(id);
    }
    public Boolean exist(long id){
        return repository.existsById(id);
    }

    public void delete(long id){
        repository.deleteById(id);
    }
}
