package edu.universiapolis.hdc.config;

import javassist.NotFoundException;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.security.InvalidParameterException;

@ControllerAdvice
public class GlobalErrorHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity notFound(NotFoundException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(InvalidParameterException.class)
    public ResponseEntity notFound(InvalidParameterException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.BAD_REQUEST);
    }
}
