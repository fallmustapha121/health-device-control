package edu.universiapolis.hdc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.access.event.AbstractAuthorizationEvent;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;
import org.springframework.web.accept.ContentNegotiationStrategy;
import org.springframework.web.accept.HeaderContentNegotiationStrategy;

public class RessourceServerConfig extends ResourceServerConfigurerAdapter {

}
