package edu.universiapolis.hdc.repositories;

import edu.universiapolis.hdc.model.Respirator;
import edu.universiapolis.hdc.model.Staff;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RespiratorRepository extends PagingAndSortingRepository<Respirator,Long> {
    Page<Respirator> findAll(Pageable pageable);
    Page<Respirator> findAllByName(String name, Pageable pageable);
}
