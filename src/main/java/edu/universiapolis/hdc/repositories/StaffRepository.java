package edu.universiapolis.hdc.repositories;

import edu.universiapolis.hdc.model.Doctor;
import edu.universiapolis.hdc.model.Hopital;
import edu.universiapolis.hdc.model.Staff;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffRepository extends PagingAndSortingRepository<Staff,Long> {
    Page<Staff> findAll(Pageable pageable);
    Page<Staff> findAllByFirstNameOrLastName(String firstName, String lastName, Pageable pageable);
}
