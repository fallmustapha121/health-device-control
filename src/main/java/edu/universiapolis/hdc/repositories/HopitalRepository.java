package edu.universiapolis.hdc.repositories;

import edu.universiapolis.hdc.model.Hopital;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

@org.springframework.stereotype.Repository
public interface HopitalRepository extends PagingAndSortingRepository<Hopital,Long> {
    Page<Hopital> findAll(Pageable pageable);
    Page<Hopital> findAllByNom(String name,Pageable pageable);
}
