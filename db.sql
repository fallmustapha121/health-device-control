-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: hdcDB
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authority`
--

DROP TABLE IF EXISTS `authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authority` (
  `id` int NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `authority_UNIQUE` (`authority`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authority`
--

LOCK TABLES `authority` WRITE;
/*!40000 ALTER TABLE `authority` DISABLE KEYS */;
INSERT INTO `authority` VALUES (1,'staff_write');
/*!40000 ALTER TABLE `authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hopital`
--

DROP TABLE IF EXISTS `hopital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hopital` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `telephone` bigint NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hopital`
--

LOCK TABLES `hopital` WRITE;
/*!40000 ALTER TABLE `hopital` DISABLE KEYS */;
INSERT INTO `hopital` VALUES (38,'Hassan2','Avenue Hassan 2',511522312,'Hassan2@sante.com');
/*!40000 ALTER TABLE `hopital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `sex` varchar(45) NOT NULL,
  `age` int DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `entry_date` date DEFAULT NULL,
  `pathology` varchar(255) DEFAULT NULL,
  `room` varchar(255) DEFAULT NULL,
  `hopital_id` bigint NOT NULL,
  `doctor` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idpatient_UNIQUE` (`id`),
  KEY `fk_patient_hopital1_idx` (`hopital_id`),
  KEY `fk_patient_staff1_idx` (`doctor`),
  CONSTRAINT `fk_patient_hopital1` FOREIGN KEY (`hopital_id`) REFERENCES `hopital` (`id`),
  CONSTRAINT `fk_patient_staff1` FOREIGN KEY (`doctor`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respirator`
--

DROP TABLE IF EXISTS `respirator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `respirator` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_time` date DEFAULT NULL,
  `extinction_time` date DEFAULT NULL,
  `room` varchar(255) DEFAULT NULL,
  `state` tinyint DEFAULT NULL,
  `volume` float DEFAULT NULL,
  `pressure` float DEFAULT NULL,
  `time` time DEFAULT NULL,
  `pep` float DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `hopital_id` bigint NOT NULL,
  `patient_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_respirator_hopital1_idx` (`hopital_id`),
  KEY `fk_respirator_patient1_idx` (`patient_id`),
  CONSTRAINT `fk_respirator_hopital1` FOREIGN KEY (`hopital_id`) REFERENCES `hopital` (`id`),
  CONSTRAINT `fk_respirator_patient1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respirator`
--

LOCK TABLES `respirator` WRITE;
/*!40000 ALTER TABLE `respirator` DISABLE KEYS */;
INSERT INTO `respirator` VALUES (1,'DGB',NULL,NULL,'DG',0,0,0,NULL,0,NULL,38,NULL),(2,'LGYU',NULL,NULL,'KGJ',0,0,0,NULL,0,NULL,38,NULL),(3,'qds',NULL,NULL,'qf',0,0,0,NULL,0,NULL,38,NULL),(4,'dqf',NULL,NULL,'qdf',0,0,0,NULL,0,NULL,38,NULL),(5,'ygjg',NULL,NULL,'jhvgj',0,0,0,NULL,0,NULL,38,NULL),(6,'cwx',NULL,NULL,'qsf',0,0,0,NULL,0,NULL,38,NULL),(7,'fb',NULL,NULL,'db',0,0,0,NULL,0,NULL,38,NULL);
/*!40000 ALTER TABLE `respirator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `sex` varchar(45) NOT NULL,
  `phone` bigint DEFAULT NULL,
  `age` int DEFAULT NULL,
  `hopital_id` bigint NOT NULL,
  `user_id` bigint DEFAULT NULL,
  `fonction` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_staff_hopital1_idx` (`hopital_id`),
  KEY `fk_staff_user1_idx` (`user_id`),
  CONSTRAINT `fk_staff_hopital1` FOREIGN KEY (`hopital_id`) REFERENCES `hopital` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_staff_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (13,'test','test','M',65,54,38,20,NULL,'dsqf','dsqf','doctor'),(14,'Aidar','Aly','M',5425,54,38,21,NULL,'chirurgie','Medcin en chef','doctor');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_has_patient`
--

DROP TABLE IF EXISTS `staff_has_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_has_patient` (
  `staff_id` bigint NOT NULL,
  `patient_id` bigint NOT NULL,
  PRIMARY KEY (`staff_id`,`patient_id`),
  KEY `fk_staff_has_patient_patient1_idx` (`patient_id`),
  KEY `fk_staff_has_patient_staff1_idx` (`staff_id`),
  CONSTRAINT `fk_staff_has_patient_patient1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`),
  CONSTRAINT `fk_staff_has_patient_staff1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_has_patient`
--

LOCK TABLES `staff_has_patient` WRITE;
/*!40000 ALTER TABLE `staff_has_patient` DISABLE KEYS */;
/*!40000 ALTER TABLE `staff_has_patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `enabled` tinyint NOT NULL,
  `locked` tinyint DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'{bcrypt}$2a$10$MF7hYnWLeLT66gNccBgxaONZHbrSMjlUofkp50sSpBw2PJjUqU.zS','test','test',1,0,'test@test.com',NULL),(2,'{bcrypt}$2a$10$FzJsX1mHI.Trsh/TTqG9VuDg60n3y2mkSEUIn0tqXFNACfExk8Bke','qfg','sqf',1,0,'qfd@qsfd.fd',NULL),(3,'qPuBJ','qsdf','sdfq',1,0,'qdf@qdsf.qsdf',NULL),(4,'NPuH4','fsdq','sqfd',1,0,'t@t.com',NULL),(6,'{bcrypt}$2a$10$DD3Sln4a8SytPhnQ1L88leU7Rd4sBqbh42NzE4YuUeG3RaF6.5Y6.','dqfd','sqdf',1,0,'tt@tt.com',NULL),(7,'{bcrypt}$2a$10$TEXtIumnTHQ4i.Uq0WwHRODB4/jzCImybV31SquzAQFo5y5Z4T2Le','sdqfdq','sdfq',1,0,'pr@p.com',NULL),(8,'{bcrypt}$2a$10$PJwgXrUJ.mQaXmkVZa53HuzbDZAEST69NqPrnliuBsbChrNe8WLQS','sdqf','sqf',1,0,'dq@df.sf',NULL),(9,'{bcrypt}$2a$10$I1URAJ3FhkrT64EAoWD4G.osz.WAIdf1szTzC4YjtdDv.nX/qXmle','sqf','sdqf',1,0,'fds@fds',NULL),(10,'{bcrypt}$2a$10$OHdrkuUKQA4ckuY.NeVGUeZLYyreU3hKqik7.rMLHimXPDyG7YRlm','grsgs','dh',1,0,'dth@gc',NULL),(11,'{bcrypt}$2a$10$LDu7iyRRMJBoHo2J2.2yd.Khq0oYNwR2oZ1hyqPS5qjkxvOqh4Z7m','kljn','lnln',1,0,'konj@nkk',NULL),(12,'{bcrypt}$2a$10$F8iuAolPyGnVmCQxZWROGuirpYWSw.GyGoNMrPVUUe4MPzVfrM.5e','sfv','fvg',1,0,'sqdf@qsdf',NULL),(13,'{bcrypt}$2a$10$iL5KMtI.vdHWD7i0Zk6NLOGQBrfLlr3TBT6aE9218vL88NsrvmnXG','hfhg','jv',1,0,'jkb@g',NULL),(14,'{bcrypt}$2a$10$066AnLcg.CZON6VitRRNp.zU0PDYh5Ya0xr5fw1sFS3AH4yfwVjcW','md','md',1,0,'md@s',NULL),(15,'{bcrypt}$2a$10$ykSrN2NnGYMQG3nVkfEHRe3r1vWpnpUYrvUWZesJZTb8dBeOqA2Mi','md','lkn',1,0,'qsdđ@qf',NULL),(16,'{bcrypt}$2a$10$304KLrlQlLhRxtlOe0hPPuIUbRhEQVQ1Odl6t1JajOIkKSLcBbDE2','fsqdg','sdqf',1,0,'qsf@ßqdf',NULL),(17,'{bcrypt}$2a$10$oyd/igBannc9Pre.vKfW7ukUSAKSk8zz3n9AmQs4SX7avg8WzDFUK','dfv','fxb',1,0,'gdh@hf',NULL),(18,'{bcrypt}$2a$10$PVHZpXJBhHeUywJMu2YIXeG6Ikbtfun4mPuqdlIqYR2.YNzm9/R1.','testi','sqdf',1,0,'qdsf@df',NULL),(19,'{bcrypt}$2a$10$GEj/McFWAGfDXwVNqAUYvumT.9cckOOyaAlyLfEV0ndy.OGPRTgvK','testi2','sqfd',1,0,'testi@testi.com',NULL),(20,'{bcrypt}$2a$10$hTUN.KXrMR4Q0pDotm.ajeGVMo/H3179H4VEK3PBrK0nE2X9YywBq','test','test',1,0,'tess@sf.sf',NULL),(21,'{bcrypt}$2a$10$7uSwMB/5wEOqrqIRBuCE2ebeZ0ci2RXrQxQKDpGpWizk2qEKf7LmC','Aly','Aidar',1,0,'sqf@fd',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_authority`
--

DROP TABLE IF EXISTS `user_has_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_has_authority` (
  `user_id` bigint NOT NULL,
  `authority_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`authority_id`),
  KEY `fk_user_has_authority_authority1_idx` (`authority_id`),
  KEY `fk_user_has_authority_user_idx` (`user_id`),
  CONSTRAINT `fk_user_has_authority_authority1` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`),
  CONSTRAINT `fk_user_has_authority_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_authority`
--

LOCK TABLES `user_has_authority` WRITE;
/*!40000 ALTER TABLE `user_has_authority` DISABLE KEYS */;
INSERT INTO `user_has_authority` VALUES (1,1);
/*!40000 ALTER TABLE `user_has_authority` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-11  0:22:51
